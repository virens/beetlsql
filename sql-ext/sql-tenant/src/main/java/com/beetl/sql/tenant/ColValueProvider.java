package com.beetl.sql.tenant;

public interface ColValueProvider {
	public Object getCurrentValue();
}
