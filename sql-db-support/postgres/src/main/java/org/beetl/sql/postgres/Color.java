package org.beetl.sql.postgres;

import lombok.Data;

@Data
public class Color {
	String ab;
	String desc;
}
